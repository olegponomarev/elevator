package edu.mipt.ponomarev.chatfuel;

import java.security.InvalidParameterException;

/**
 * Idea was the following:
 * User calls the elevator from the inside and from the outside.
 * So there should be a difference in processing of these cases,
 * because if there are two buttons on each storey (up and down),
 * except for the first one and the last one, elevator can predict
 * the next user's command. But It's not implemented yet.
 * Now direction is assigned automatically depending on current location
 * and destination.
 */
public class Command implements Comparable<Command> {
	private static final int MIN_STOREY = 1;
	public static int MAX_STOREY;

	private Integer storey;
	private Direction direction;

	public Command(Direction direction, Integer storey) {
		validate(storey);
		this.direction = direction;
		this.storey = storey;
	}

	private void validate(Integer storey) {
		if (storey > MAX_STOREY || storey < MIN_STOREY) {
			throw new InvalidParameterException();
		}
	}

	public Command(Integer storey) {
		this(Direction.GUESS_YOURSELF, storey);
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public Integer getStorey() {
		return storey;
	}

	public void setStorey(Integer storey) {
		this.storey = storey;
	}

	@Override
	public int compareTo(Command o) {
		return this.getStorey().compareTo(o.getStorey());
	}

	enum Direction {
		UP,
		DOWN,
		GUESS_YOURSELF
	}

	@Override
	public String toString() {
		return "Command{" +
				"direction=" + direction +
				", storey=" + storey +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Command command = (Command) o;

		return direction == command.direction && storey.equals(command.storey);
	}

	@Override
	public int hashCode() {
		int result = direction.hashCode();
		result = 31 * result + storey.hashCode();
		return result;
	}
}
