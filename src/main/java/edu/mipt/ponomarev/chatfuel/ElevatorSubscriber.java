package edu.mipt.ponomarev.chatfuel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Flow;

public class ElevatorSubscriber implements Flow.Subscriber<Command> {
	private static final int NUMBER_REQUESTED = 1;
	private static final Logger LOGGER = LoggerFactory.getLogger(ElevatorSubscriber.class);

	private final ExecutorService processorExecutor;
	private final ExecutorService engineExecutor;
	private final Elevator elevator;
	private Flow.Subscription subscription;

	public ElevatorSubscriber(Elevator elevator, ExecutorService processorExecutor, ExecutorService engineExecutor) {
		this.elevator = elevator;
		this.processorExecutor = processorExecutor;
		this.engineExecutor = engineExecutor;
	}

	@Override
	public void onSubscribe(Flow.Subscription subscription) {
		this.subscription = subscription;
		subscription.request(NUMBER_REQUESTED);
		LOGGER.debug("Subscribed successfully");
	}

	@Override
	public void onNext(Command item) {
		LOGGER.debug("Got {}", item);
		processorExecutor.execute(() -> elevator.processCommand(item));
		engineExecutor.execute(elevator::go);
		subscription.request(NUMBER_REQUESTED);
	}

	@Override
	public void onError(Throwable throwable) {
		LOGGER.debug("Error!");
		subscription.cancel();
	}

	@Override
	public void onComplete() {
		LOGGER.debug("Subscription has been canceled");
		subscription.cancel();
	}
}
