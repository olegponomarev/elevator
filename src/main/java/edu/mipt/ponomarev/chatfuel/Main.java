package edu.mipt.ponomarev.chatfuel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStreamReader;
import java.security.InvalidParameterException;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SubmissionPublisher;

public class Main {
	private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
	private static final String ENTER_COMMAND = "So, what are we gonna do now?\n" +
			"Type 'exit' to shutdown or enter the storey you want the elevator to go\n";
	private static final String WRONG_INPUT = "Wrong format; try again";
	private static final String WRONG_CONFIG = "Please provide the app with the following command line arguments:\n" +
			"1. Number of storeys (5 to 20);\n" +
			"2. Height of a storey (in meters);\n" +
			"3. Velocity of the elevator (in meters per second);\n" +
			"4. Time when doors are opened (in seconds).";
	private static final String WRONG_COMMAND = "There is no such a storey in the building.\n" +
			"Actually, real word elevator API doesn't allow you to do it.";

	public static void main(String... args) {
		Elevator elevator;
		try {
			Command.MAX_STOREY = Integer.parseInt(args[0]);
			elevator = new Elevator(
					Command.MAX_STOREY,
					Double.parseDouble(args[1]),
					Double.parseDouble(args[2]),
					Double.parseDouble(args[3])
			);
		} catch (RuntimeException e) {
			LOGGER.warn(WRONG_CONFIG);
			return;
		}
		SubmissionPublisher<Command> publisher = new SubmissionPublisher<>();
		ExecutorService processorExecutor = Executors.newSingleThreadExecutor();
		ExecutorService engineExecutor = Executors.newSingleThreadExecutor();
		ElevatorSubscriber subscriber = new ElevatorSubscriber(elevator, processorExecutor, engineExecutor);
		publisher.subscribe(subscriber);

		Scanner scanner = new Scanner(new InputStreamReader(System.in));
		LOGGER.info(ENTER_COMMAND);
		while (true) {
			String next = scanner.next().toUpperCase();
			if (Objects.equals("EXIT", next)) {
				LOGGER.info("See you next time!");
				processorExecutor.shutdown();
				engineExecutor.shutdown();
				publisher.close();
				break;
			}
			try {
				int storey = Integer.parseInt(next);
				publisher.submit(new Command(storey));
			} catch (InvalidParameterException e) {
				LOGGER.warn(WRONG_COMMAND);
			} catch (RuntimeException e) {
				LOGGER.warn(WRONG_INPUT);
			}
		}
	}
}
