import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.classic.filter.ThresholdFilter

appender("STDOUT", ConsoleAppender) {
    filter(ThresholdFilter) {
        level = info
    }
    encoder(PatternLayoutEncoder) {
        pattern = "%msg%n"
    }
}

appender("file", FileAppender) {
    append = false
    file = './debug'
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n"
    }
}
root(debug, ["STDOUT", "file"])